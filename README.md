# QSimpleUpdaterTest

#### 介绍
QSimpleUpdater项目测试

可使用独立程序UpdateModel升级自己的QT程序

#### 软件架构
软件架构说明

MainApp—待更新的主程序（测试项）

UpdateModel—程序更新模块（GUI）,简单修改QSimpleUpdater项目下的示例tutorial程序


#### 安装教程

1.  拉取代码后，将3rdparty和UpdateModel目录复制到所需项目下（UpdateModel依赖3rdparty下的QSimpleUpdate项目）
2.  UpdateModel程序生成路径需要和待更新程序路径一致

#### 使用说明

1. 待更新程序（主程序）需要执行外部程序“update.exe”，并传入参数

   参数1 APP_VERSION： 主程序当前版本号（格式类似于 “1.2” ）

   参数2 PID： 主程序进程PID

   参数3 DEFS_URL:  json文件远程地址

   ```c
   #include "widget.h"
   #include <QApplication>
   #include <QProcess>
   
   const QString DEFS_URL = "http://192.168.100.127/Common/Software/Relay_Device/definitions/updates.json";
   
   int main(int argc, char *argv[])
   {
       QApplication a(argc, argv);
       Widget w;
       w.show();
   
       QString APP_VERSION = a.applicationVersion();
       QString PID = QString::number(a.applicationPid());
       QProcess updater;
       QStringList arg;
       arg << APP_VERSION << PID << DEFS_URL;
       updater.startDetached("update.exe", arg);
   
       return a.exec();
   }
   ```

   ```c
   # 在主程序.pro文件里定义程序版本号VERSION
   VERSION = "1.0.0.1"
   ```

   

2. UpdateModel/definitions下包含json示例文件，update程序通过解析json文件来获取远程程序版本号及其安装文件(压缩包)路径

   ```c
   {
     "updates": {
       "windows": {
         "open-url": "",
         "latest-version": "1.0.0.2",
         "download-url": "http://192.168.100.127/Common/Software/Relay_Device/last_release/release.zip",
         "changelog": "搭建web服务器以推送程序升级包",
         "mandatory": true
       },
       "osx": {
         "open-url": "",
         "latest-version": "1.0",
         "download-url": "https://gitee.com/api/v5/repos/{owner}/{repo}/releases/latest",
         "changelog": "This is an example changelog for Mac OS X. Go on...",
         "mandatory": true
       },
       "linux": {
         "open-url": "",
         "latest-version": "1.0",
         "download-url": "https://gitee.com/api/v5/repos/{owner}/{repo}/releases/latest",
         "changelog": "This is an example changelog for Linux. Go on...",
         "mandatory": true
       },
       "ios": {
         "open-url": "",
         "latest-version": "1.0",
         "download-url": "https://gitee.com/api/v5/repos/{owner}/{repo}/releases/latest",
         "changelog": "This is an example changelog for iOS. Go on...",
         "mandatory": true
       },
       "android": {
         "open-url": "",
         "latest-version": "1.0",
         "download-url": "https://gitee.com/api/v5/repos/{owner}/{repo}/releases/latest",
         "changelog": "This is an example changelog for Android. Go on...",
         "mandatory": true
       }
     }
   }
   ```

   

3. 远程升级包只支持压缩包(.zip )，update程序将获取到的升级包解压至

   程序所在目录，其目录下会有release文件夹生成，最后将release目录里的更新文件拷贝至程序所在目录并删除release文件夹和release.zip文件

   注意：

   2. 远程升级压缩包名为release.zip，解压至程序所在目录会有release文件夹
   2. 从release文件夹内拷贝更新文件会有文件占用问题，程序会忽略跳过更新占用文件
   3. 没有做文件(夹)对照，旧版程序若有冗余文件，更新程序也不会将其删除

   

4.  如果是采用压缩包升级，web服务器MIME类型请添加 .zip的请求头(任何格式都可以)
