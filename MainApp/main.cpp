#include "widget.h"
#include <QApplication>
#include <QProcess>

const QString DEFS_URL = "http://192.168.100.127/Common/Software/Relay_Device/definitions/updates.json";

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    w.show();

    QString APP_VERSION = a.applicationVersion();
    QString PID = QString::number(a.applicationPid());
    QProcess updater;
    QStringList arg;
    arg << APP_VERSION << PID << DEFS_URL;
    updater.startDetached("update.exe", arg);

    return a.exec();
}
