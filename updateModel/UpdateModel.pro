QT      += core gui gui-private
TEMPLATE = app
TARGET = "update"

FORMS   += Window.ui
HEADERS += Window.h
SOURCES += Window.cpp \
           main.cpp

QMAKE_LFLAGS += /MANIFESTUAC:\"level=\'requireAdministrator\' uiAccess=\'false\'\"
		   
contains(QT_ARCH, i386) {
        message("32-bit")
        CONFIG(release, debug|release){
            DESTDIR += $$PWD/../build/x86/release
        }
        CONFIG(debug, debug|release){
            DESTDIR += $$PWD/../build/x86/debug
        }
    }else {
        message("64-bit")
        CONFIG(release, debug|release){
            DESTDIR += $$PWD/../build/x64/release
        }
        CONFIG(debug, debug|release){
            DESTDIR += $$PWD/../build/x64/debug
        }
    }
		   
include($$PWD/../3rdparty/QSimpleUpdater/QSimpleUpdater.pri)
